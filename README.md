# Widget

### Modo de uso

* Copiar carpeta 'widget' en la raiz del proyecto

* Crear DIV contenedor
        

* Incluir JQUERY (SOLO SI NO LO INCLUYEN ANTES)
        

* Incluir script Widget
        

* Inicializar Widget
        

#### Parámetros para inicializar el widget:

* Modo: 'h' ó 'v'  (Para visualizar horizontal o vertical)
* Identificador externo: Código para identificar la empresa que usa el widget
* Logo: Logo que se visualizará en el sistema de venta
* Color principal
* Color secundario
* Test: para iniciar en modo de pruebas

** Todos los parametros son opcionales **
