var external_paradas = []
var url_api = '';
var session_api = '';

const consultaApi = async () => {
  const password = {
    clave_acceso: "qu3r1c4qu3s0s",
  };
  const env = sessionStorage.getItem('env');
  url_api = env === 'test' ? "https://ws.viatesting.com.ar/" : "https://ws.busplus.com.ar/";

  const session = await fetch(url_api + "sesion", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(password),
  });

  const key = await session.json();

  session_api = key.key;
  // Segundo llamado API
  const paradas = await fetch(url_api + "paradas", {
      method: "GET",
      headers: {
        "X-API-KEY": key.key,
        "Content-Type": "application/json",
      },
    }
  );

  const data = await paradas.json();
  data.forEach(paradas => {
    external_paradas.push(paradas)
  });


};


consultaApi();
